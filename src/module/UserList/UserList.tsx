import { FunctionComponent, useEffect, useState } from "react";
import { Link } from "../../components/Button/Button";
import { InputText } from "../../components/InputText/InputText";
import { Page } from "../../components/Page/Page";
import {
  Table,
  TableBody,
  TableHead,
  TableItem,
  TableRow,
} from "../../components/Table";
import { useUsers } from "./hooks/useUser";
import { User } from "./model/User";
import { processUsers, UserProcessor } from "./processor";

export const UserList: FunctionComponent = () => {
  const { users, isLoading, error } = useUsers();
  const [processedUsers, setFilterUsers] = useState<
    UserProcessor[] | undefined
  >(undefined);

  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    setFilterUsers(processUsers(users as User[]));
  }, [users]);

  const filterUsers = (value: string) => {
    const filteredUsers = value
      ? users?.filter(
          (user) =>
            user.name.toLowerCase().includes(value.toLowerCase()) ||
            user.email.toLowerCase().includes(value.toLowerCase())
        )
      : undefined;

    setFilterUsers(processUsers(filteredUsers || (users as User[])));
    setSearchTerm(value);
  };

  return (
    <Page loading={isLoading} error={error}>
      <InputText onChange={filterUsers} />
      <Table>
        <TableHead
          fields={["Name", "Address", "Phone", "Email", "Website", "Edit"]}
        />
        <TableBody>
          {processedUsers?.map((user) => (
            <TableRow key={`${user.id}`}>
              <TableItem highlight={searchTerm}>{user.name}</TableItem>
              <TableItem>{user.addressName}</TableItem>
              <TableItem>{user.phone}</TableItem>
              <TableItem highlight={searchTerm}>{user.email}</TableItem>
              <TableItem>{user.website}</TableItem>
              <TableItem>
                <Link href={"/" + user.id + "/posts"}>See Posts</Link>
              </TableItem>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Page>
  );
};

UserList.displayName = "UserList";

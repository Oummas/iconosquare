import { User } from "./model/User";

export interface UserProcessor {
  id: number;
  name: string;
  addressName: string;
  phone: string;
  email: string;
  website: string;
}
export const processUsers = (users: User[]):UserProcessor[] =>
  users?.map((user: User) => ({
    id: user.id,
    name: user.name,
    addressName:
      user.address.street +
      " " +
      user.address.suite +
      " " +
      user.address.city +
      " " +
      user.address.zipcode,
    phone: user.phone,
    website: user.website,
    email: user.email
  }));

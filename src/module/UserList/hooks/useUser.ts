import axios, { AxiosResponse } from "axios";
import useSWR from "swr";
import { User } from "../model/User";

interface UserHook {
  users: User[] | undefined;
  error: Error | undefined;
  isLoading: boolean;
}
export const useUsers = (): UserHook => {
  const { data, error } = useSWR<AxiosResponse<User[]>>(
    `https://jsonplaceholder.typicode.com/users`,
    axios,{
      revalidateOnFocus: false,

    }
  );

  return {
    users: data?.data,
    isLoading: !error && !data,
    error,
  };
};

import { render } from "@testing-library/react";
import { UserList } from "../UserList";

jest.mock("../hooks/useUser", () => ({
  useUsers: () => ({
    users: require('./__mock__/users.mock.ts').usersMocks,
    isLoading: false,
  }),
}));

describe("UserList", () => {
  it("should display user list", () => {
    const { container } = render(<UserList />);
    expect(container).toMatchSnapshot();
  });
});

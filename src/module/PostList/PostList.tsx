import { AxiosResponse } from "axios";
import { createContext, useCallback, useState } from "react";
import { KeyedMutator } from "swr";
import { Modal } from "../../components/Modal/Modal";
import { Page } from "../../components/Page/Page";
import { CommentList } from "../CommentList/CommentList";
import { CreatePost } from "./components/CreatePost/CreatePost";
import { PostItem } from "./components/PostItem";
import { useUserPosts } from "./hooks/useUserPosts";
import { Post as PostModel } from "./models/Post";

export const PostContext = createContext<
  | {
      posts: PostModel[] | undefined;
      addPost: KeyedMutator<AxiosResponse<PostModel[], any>>;
    }
  | undefined
>(undefined);

export const PostList = () => {
  const { posts, isLoading, error, mutate } = useUserPosts();

  const [modalPost, setModalPost] = useState<PostModel | null>(null);

  const handleClickOnPost = useCallback(
    (id: number) => {
      setModalPost(posts?.find((post) => post.id === id) || null);
    },
    [posts]
  );
  const handleCloseModal = () => {
    setModalPost(null);
  };

  return (
    <Page loading={isLoading} error={error}>
      <div className="grid grid-cols-1 md:grid-cols-3">
        <PostContext.Provider value={{ posts, addPost: mutate }}>
          <CreatePost />
        </PostContext.Provider>
        {posts?.map((post) => (
          <PostItem
            key={post.id}
            id={post.id}
            title={post.title}
            body={post.body}
            onClick={handleClickOnPost}
          />
        ))}
      </div>
      <Modal title="" open={!!modalPost} onClose={handleCloseModal}>
        <div className="p-4">
          <div className="font-bold text-xl mb-4">{modalPost?.title}</div>
          <p className="text-black-600">{modalPost?.body}</p>
        </div>
        <hr />
        {modalPost && <CommentList postId={modalPost?.id} />}
      </Modal>
    </Page>
  );
};

PostList.displayName = "PostList";

import axios from "axios";
import { useContext } from "react";
import { useParams } from "react-router";
import { useSWRConfig } from "swr";
import { PostContext } from "../PostList";

interface PostForm {
  id?: number;
  title: string;
  body: string;
  userId?: number;
}
export const useAddPost = () => {
  const post = useContext(PostContext);
  const { userId } = useParams();
  const { mutate } = useSWRConfig();

  const savePost = async (postForm: Pick<PostForm, "title" | "body">) => {
    const posts = post?.posts ?? [];

    const {
      data: { id },
    } = await axios.post<{ id: string }>(
      `https://jsonplaceholder.typicode.com/posts`,
      postForm
    );
    mutate(
      `https://jsonplaceholder.typicode.com/users/${userId}/posts`,
      { data: [{ id, ...postForm }, ...posts] },
      false
    );
  };

  return { savePost };
};

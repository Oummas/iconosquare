import axios, { AxiosResponse } from "axios";
import { useParams } from "react-router";
import useSWR from "swr";
import { Post } from "../models/Post";

export const useUserPosts = () => {
  const { userId } = useParams();
  const { data, error, mutate } = useSWR<AxiosResponse<Post[]>>(
    `https://jsonplaceholder.typicode.com/users/${userId}/posts`,
    axios
  );

  return { posts: data?.data, error, isLoading: !error && !data, mutate };
};

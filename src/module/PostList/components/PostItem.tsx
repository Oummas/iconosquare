import React from "react";

interface PostItemProps {
  id: number;
  title: string;
  body: string;
  onClick: (id: number) => void;
}

export const PostItem = React.memo<PostItemProps>(
  ({ id, title, body, onClick }) => {
    const handleClick = () => { onClick(id); };
    return (
      <div
        key={id}
        onClick={handleClick}
        className="bg-white rounded-lg p-6 shadow-sm flex justify-between flex-col cursor-pointer hover:shadow-md"
        style={{ transformOrigin: "50% 50% 0px" }}
      >
        <div>
          <h3 className="font-bold text-xl mb-4">{title}</h3>
          <p className="text-gray-600">{body}</p>
        </div>
      </div>
    );
  }
);

PostItem.displayName = "PostItem";

import React, { FunctionComponent } from "react";
import { useAddPost } from "../../hooks/useAddPost";

interface AddPostProps {
  onSave: () => void;
}

export const AddPost:FunctionComponent<AddPostProps> = ({ onSave }) => {
  const [postForm, setPostForm] = React.useState({
    title: "",
    body: "",
  });
  const { savePost } = useAddPost();

  return (
    <div className="p-2 w-full">
      <div className="font-bold text-xl mb-2">Create New Post</div>
      <div className="mb-6">
        <label className="block text-gray-700 text-sm text-left font-bold mb-2">
          Title
        </label>
        <input
          className="shadow appearance-none border rounded w-full py-2 px-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="title"
          type="text"
          placeholder="Title"
          onChange={(e) => setPostForm({ ...postForm, title: e.target.value })}
        />
      </div>
      <div className="mt-6 text-left">
        <label htmlFor="body">Content</label>
        <textarea
          id="body"
          name="body"
          className="appearance-none mt-1 rounded relative block w-full px-3 py-3 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
          onChange={(e) => setPostForm({ ...postForm, body: e.target.value })}
        ></textarea>
      </div>
      <div className="mt-8">
        <button
          type="submit"
          className="group relative w-full flex justify-center py-3 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={() => {
            savePost(postForm);
            onSave();
          }}
        >
          Create
        </button>
      </div>
    </div>
  );
};

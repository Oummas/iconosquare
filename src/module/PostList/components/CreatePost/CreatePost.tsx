import React, { useState } from "react";
import { Modal } from "../../../../components/Modal/Modal";
import { AddPost } from "./AddPost";

export const CreatePost = () => {
  const [openModal, setOpenModal] = useState(false);
  return (
    <div
      onClick={() => setOpenModal(true)}
      className="rounded-lg p-6 flex flex-col items-center justify-center shadow-sm border-2 border-dashed text-gray-500 cursor-pointer hover:bg-white"
    >
      <svg x="0px" y="0px" className="w-8 h-8 mb-4" viewBox="0 0 512 512">
        <path
          fill="currentColor"
          d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z"
        ></path>
      </svg>
      <p>Create new post</p>
      <Modal
        title=""
        open={openModal}
        onClose={() => {
          setOpenModal(false);
        }}
      >
       <AddPost onSave={() =>  setOpenModal(false) }/>
      </Modal>
    </div>
  );
};

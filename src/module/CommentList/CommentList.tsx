import React, { FunctionComponent } from "react";
import { usePostComment } from "./hooks/usePostComment";

interface CommentListProps {
  postId: number;
}

export const CommentList: FunctionComponent<CommentListProps> = ({
  postId,
}) => {
  const { comments } = usePostComment(postId);
  return comments?.length ? (
    <>
      <div className="title text-left text-xl underline">Commentaire:</div>
      <div className="grid grid-cols-1 divide-y divide-gray-500 gap-5">
        {comments?.map((comment, index) => (
          <div className="separator" key={index}>
            <div className="text-center text-lg">{comment.name}</div>
            <div className="text-left underline">{comment.email}</div>
            <div className="text-left">{comment.body}</div>
          </div>
        ))}
      </div>
    </>
  ) : (
    <div className="title text-left">Aucun commentaire</div>
  );
};

CommentList.displayName = "CommentList";

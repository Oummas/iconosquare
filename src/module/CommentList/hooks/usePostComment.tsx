import axios, { AxiosResponse } from "axios";
import useSWR from "swr";

export const usePostComment = (postId: number) => {
  const { data, error } = useSWR<AxiosResponse<any[]>>(
    `https://jsonplaceholder.typicode.com/posts/${postId}/comments`,
    axios
  );
  return { comments: data?.data, error, isLoading: !error && !data };
};

 import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { PostList } from "./module/PostList/PostList";
import { UserList } from "./module/UserList/UserList";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/:userId/posts" element={<PostList />}/>
        <Route path="/" element={<UserList />} />
      </Routes>
    </Router>
  );
}

export default App;

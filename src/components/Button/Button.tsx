import React, { FunctionComponent } from "react";

export interface LinkButtonProps {
  href: string;
}

export const Link: FunctionComponent<LinkButtonProps> = ({
  children,
  href,
}) => {
  return (
    <a
      href={href}
      className="bg-blue-light rounded-lg p-3 shadow-md hover:bg-blue-darker"
    >
      {children}
    </a>
  );
};

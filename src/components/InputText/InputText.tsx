import React, { FunctionComponent } from "react";

interface InputTextProps {
  onChange?: (value: string) => void;
}

export const InputText: FunctionComponent<InputTextProps> = ({ onChange }) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      onChange(e.target.value);
    }
  };
  return (
    <div className="flex relative mx-auto w-full">
      <input
        autoComplete="off"
        className="border-2 border-primary bg-red transition h-12 px-5 pr-16 rounded-md focus:outline-none w-full text-black text-lg "
        type="search"
        name="search"
        placeholder="Search"
        onChange={handleChange}
      />
      <button type="submit" className="absolute right-2 top-3 mr-4" />
    </div>
  );
};

InputText.displayName = "InputText";

import React, { FunctionComponent } from "react";
import { Spinner } from "../Spinner/Spinner";

interface PageProps {
  loading?: boolean;
  error?: Error;
}
export const Page: FunctionComponent<PageProps> = ({
  children,
  loading = false,
  error,
}) => {
  if (loading) return <Spinner />;
  if (error) return <div>{error.message}</div>;
  return <div>{children}</div>;
};

Page.displayName = "Page";

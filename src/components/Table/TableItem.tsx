import { FunctionComponent } from "react";

export interface TableItemProps {
  highlight?: string;
}
export const getHighlightedText = (text: string, highlight: string) => {
  const parts = text.split(new RegExp(`(${highlight})`, "gi"));
  return (
    <span>
      {parts.map((part, i) => (
        <span
          key={i}
          style={
            part.toLowerCase() === highlight.toLowerCase()
              ? { backgroundColor: "yellow" }
              : {}
          }
        >
          {part}
        </span>
      ))}{" "}
    </span>
  );
};

const TableItem: FunctionComponent<TableItemProps> = ({
  children,
  highlight,
}) => {
  return (
    <td className="px-6 py-4 whitespace-nowrap">
      {highlight && typeof children === "string"
        ? getHighlightedText(children as string, highlight)
        : children}
    </td>
  );
};
TableItem.displayName = "TableItem";

export default TableItem;

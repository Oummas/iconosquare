import { render } from "@testing-library/react";
import { Table } from "..";

describe("Test table", () => {
  it("should return snapshot with table", () => {
    const { getByTestId } = render(<Table />);
    expect(getByTestId("table")).toBeDefined();
  });
  it("should return snapshot with children", () => {
    const { getByText } = render(
      <Table>
        <tbody>
          <tr>
            <th>TEST CHILDREN</th>
          </tr>
        </tbody>
      </Table>
    );
    expect(getByText("TEST CHILDREN")).toBeDefined();
  });
});

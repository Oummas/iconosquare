import React, { FunctionComponent } from "react";

const TableRow: FunctionComponent = ({ children }) => {
  return <tr>{children}</tr>;
};
TableRow.displayName = "TableRow";
export default TableRow;

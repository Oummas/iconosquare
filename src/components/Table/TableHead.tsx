import React, { FunctionComponent } from "react";

interface TableHeadProps {
  fields: string[];
}
const TableHead: FunctionComponent<TableHeadProps> = ({ fields }) => {
  return (
    <thead className="bg-gray-50">
      <tr>
        {fields.map((field, index) => (
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            key={`${field}-${index}`}
          >
            {field}
          </th>
        ))}
      </tr>
    </thead>
  );
};

TableHead.displayName = "TableHead";

export default TableHead;

import React, { FunctionComponent } from "react";

const TableBody: FunctionComponent = ({ children }) => {
  return (
    <tbody className="bg-white divide-y divide-gray-200">{children}</tbody>
  );
};

TableBody.displayName = "TableBody";

export default TableBody;
